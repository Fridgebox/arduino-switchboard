#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_BME280.h>

struct ACVoltageRegulator
{
public:
  
  enum state
  { manual = -2
  , off = -1
  , level1 = 0
  , level2
  , level3
  , level4
  , full
  };
  
private:  
  constexpr static uint8_t override_switch_pin = 2;
  
  enum override_switch
  { MANUAL      = HIGH
  , RELAIS_BANK = LOW
  };
  
  constexpr static uint8_t PROGMEM voltage_toggle_pin[5] = {4, 5, 6, 7, 8};
  
  enum voltage_toggle 
  { ON = HIGH
  , OFF = LOW
  };

  state state_;

  void toggle_off()
  {
    if( state_ >= 0 )
    {
      digitalWrite( voltage_toggle_pin[state_], voltage_toggle::OFF );   
      state_ = state::off;
    }
  }

public:

  ACVoltageRegulator()
  : state_{ state::manual }
  {}

  void setup()
  {
    pinMode( override_switch_pin, OUTPUT);
    
    digitalWrite( override_switch_pin, override_switch::MANUAL );
  
    for
    ( uint8_t i = 0
    ; i < sizeof(voltage_toggle_pin) / sizeof(voltage_toggle_pin[0])
    ; ++i 
    )  
    {
      pinMode( voltage_toggle_pin[i], OUTPUT);
      
      digitalWrite( voltage_toggle_pin[i], voltage_toggle::OFF );
    }
  }
  
  void set( state const & new_state )
  {
    if( new_state == state_ ) return;
    
    toggle_off();

    Serial.print( F("State ") );
    Serial.println( new_state );
    
    if( new_state == state::manual )
    {
      digitalWrite( override_switch_pin, override_switch::MANUAL );
    }
    else
    {
      digitalWrite( override_switch_pin, override_switch::RELAIS_BANK );
      
      if( new_state >= 0 )  
      {
        digitalWrite( voltage_toggle_pin[ new_state ], voltage_toggle::ON );
      }
    }

    state_ = new_state;
  }

  state get_next_state() { return state_ == state::full ? state::manual : state_ + 1 ; }

  constexpr char get_state_name()
  {
    switch( state_ )
    {
      case state::manual: return 'M';
      default           : return '0' + state_ + 1;
    }
  }
};

constexpr const uint8_t ACVoltageRegulator::voltage_toggle_pin[5];

// Screen SSD1306

constexpr int const screen_width    =  128; // OLED display width, in pixels
constexpr int const screen_height   =   54; // OLED display height, in pixels
constexpr int const oled_reset_pin  =   -1; // Reset pin # (or -1 if sharing Arduino reset pin)
constexpr int const display_address = 0x3C; ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32

Adafruit_SSD1306 display(screen_width, screen_height, &Wire, oled_reset_pin);

// Voltage Regulator

ACVoltageRegulator fan_psu;

// BME 280

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;
Adafruit_BME280 bme_up;

// MQ2

// PZEM 004T

#include <PZEM004Tv30.h>

/* Use software serial for the PZEM
 * Pin 11 Rx (Connects to the Tx pin on the PZEM)
 * Pin 12 Tx (Connects to the Rx pin on the PZEM)
*/
PZEM004Tv30 pzem_fan(10, 9, 0x07 );
PZEM004Tv30 pzem_lights(10, 9, 0x08 );

void setup() 
{
  Serial.begin( 115200 );
  
  fan_psu.setup();

  // SSD 1306
  
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if( ! display.begin( SSD1306_SWITCHCAPVCC, display_address ) )
  {
    Serial.println( F("SSD1306 allocation failed") );
    for(;;); // Don't proceed, loop forever
  }

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  //display.display();

  display.cp437(true);         // Use full 256 char 'Code Page 437' font  
  display.setRotation(3);

  // BME 280

  if( ! bme.begin(0x76, &Wire) )
  {
    Serial.println( F("Could not find BME280 sensor") );
    for(;;);
  }

  if( ! bme_up.begin(0x77, &Wire) )
  {
    Serial.println( F("Could not find BME280 second sensor") );
    for(;;);
  }

}

uint8_t nan_to_null( uint8_t const& val )
{
  return isnan( val ) ? 0 : val;
}

void display_at_line( uint8_t line_no )
{
  display.setCursor( 4, ( line_no - 1 ) * 12 );
  display.setTextColor( SSD1306_WHITE ); // Draw white text    
  display.setTextSize( 1 );
}

uint8_t counter = 0;

void loop()
{
  constexpr unsigned short int const step_delay = 10;

  display.clearDisplay();

  display_at_line( 1 );
  display.print( bme.readTemperature() );
  display.print( char(248) );
  display.print( F( "C" ) );
  
  display_at_line( 2 );
  display.print( bme.readHumidity() );
  display.print( F(" %") );

  display_at_line( 3 );
  display.print( bme.readPressure() / 100 );
  display.print( F(" hPa") );

  display_at_line( 4 );
  display.print( analogRead( A1 )/*mq2.readSensor()*/ ); // middle
  display.print( F(" ") );  
  display.print( analogRead( A0 ) ); // up
  
  display_at_line( 5 );
  display.print( nan_to_null( pzem_fan.voltage() ) );
  display.print( F(" V") );

  display_at_line( 6 );
  display.print( nan_to_null( pzem_lights.voltage() ) );
  display.print( F(" A") );

  display_at_line( 7 );
  display.print( nan_to_null( pzem_fan.power() ) );
  display.print( F(" W") );

  display.setCursor( 20, 96 );
  display.setTextSize( 2 );
  display.print( fan_psu.get_state_name() );

  display.display();

  delay( step_delay);

  if( ! ++counter )
    fan_psu.set( fan_psu.get_next_state() );

  Serial.println( counter );
}
