# pragma once

# include "common.hpp"

struct ACVoltageRegulator
{
  public:

    enum class state : int8_t
    { manual = -2
    , off = -1
    , level1 = 0
    , level2
    , level3
    , level4
    , full
    };

  private:
    constexpr static uint8_t override_switch_pin = 2;
    constexpr static uint8_t voltage_toggle_pin[5] = { 8, 7, 6, 5, 4 };

    enum override_switch
    { MANUAL      = HIGH
    , RELAIS_BANK = LOW
    };

    enum voltage_toggle
    { ON = HIGH
    , OFF = LOW
    };

    state state_;

    void toggle_off()
    {
      if ( state_ >= state::off )
      {
        digitalWrite( voltage_toggle_pin[ static_cast< int8_t >(state_)], voltage_toggle::OFF );
        state_ = state::off;
      }
    }

  public:

    ACVoltageRegulator()
    : state_{ state::manual }
    {}

    void setup()
    {
      pinMode( override_switch_pin, OUTPUT);

      digitalWrite( override_switch_pin, override_switch::MANUAL );

      for
      ( uint8_t i = 0
      ; i < sizeof(voltage_toggle_pin) / sizeof(voltage_toggle_pin[0])
      ; ++i
      )
      {
        pinMode( voltage_toggle_pin[i], OUTPUT);

        digitalWrite( voltage_toggle_pin[i]
                    , voltage_toggle::OFF
                    );
      }
    }

    void set( state const & new_state )
    {
      if ( new_state == state_ ) return;

      toggle_off();

      if ( new_state == state::manual )
      {
        digitalWrite( override_switch_pin
                      , override_switch::MANUAL
                    );
      }
      else
      {
        digitalWrite( override_switch_pin
                      , override_switch::RELAIS_BANK
                    );

        if ( new_state > state::off )
        {
          digitalWrite( voltage_toggle_pin[ static_cast< int8_t >( new_state ) ]
                        , voltage_toggle::ON
                      );
        }
      }

      state_ = new_state;
    }

    void set( char const state_char )
    {
      set( state_from_name( state_char ) );
    }


    state get_state() const 
    {
      return state_;
    }

    static state state_from_name( char const state_char )
    {
      if ( state_char == 'M' )
      {
        return state::manual;
      }

      state const res_state = static_cast< state >( state_char - '0' - 1 );
      if( res_state >= state::off && res_state <= state::full )
        return res_state ;
     
      
      error( F("Invalid state name") );

      return state::manual;
    }

    char get_state_name() const
    {
      switch( state_ )
      {
        case state::manual: return 'M';
        default           : return '0' + static_cast< char >( state_ ) + 1;
      }
    }

};
