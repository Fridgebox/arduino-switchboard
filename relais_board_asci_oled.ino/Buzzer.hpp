# pragma once

# include "common.hpp"

struct Buzzer
{
  static constexpr uint8_t const pin = A2;

  bool silent_mode = false;

  void setup()
  {
    pinMode( pin, OUTPUT );
    digitalWrite( pin, HIGH );
  };

  void activate_for( unsigned milliseconds )
  {
    buzz_end_ = millis() + milliseconds;
  }

  void activate()
  {
    buzz_end_ = unsigned_long_max;
  }

  void deactivate()
  {
    buzz_end_ = 0;
  }

  void loop()
  {
    digitalWrite( pin, ! silent_mode && millis() < buzz_end_ ? LOW : HIGH );
  }
private:
  unsigned long buzz_end_ = 0;
};
