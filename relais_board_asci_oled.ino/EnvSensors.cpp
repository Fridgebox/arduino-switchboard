# include "EnvSensors.hpp"

# include "System.hpp"

template< typename T, typename Reporter >
EnvSensors::Value< T > create( T const & value, Reporter const& reporter, System & sys )
{
  System::report const report = reporter( value );
  
  sys.check( report );
  
  return { value,  bool( report ) };
};

EnvSensors::Values
EnvSensors::update
( uint8_t const env_idx
, System & sys
)
{
  mq2.update();

  return Values
  { create
    ( bme.readTemperature()
    , [&env_idx]( float const & t )
      {
        return System::report
        { "overheat"
        , env_idx
        , t > EnvSensors::temp_alert_threshold
          ? System::action::message | System::action::power_off | System::action::buzz
          : System::action::none
        };
      }
    , sys
    )
  , create
    ( bme.readHumidity()
    , [&env_idx]( float const & h )
      {
        return System::report
        { "condensation"
        , env_idx
        , h > humidity_alert_threshold
          ? System::action::message 
          : System::action::none
        };
      }
    , sys
    )
  , Value< float >{ bme.readPressure() / 100.f, false }
  , create
    ( mq2.readSensor()
    , [&env_idx]( float const & g )
      { return System::report
        { "gas_lpg"
        , env_idx
        , millis() > gas_lpg_warmup_millis
          && g > EnvSensors::gas_lpg_alert_threshold
          ? System::action::message | System::action::power_off | System::action::buzz
          : System::action::none
        };
      }
    , sys
    )
  , Value< float >{ pzem.voltage(), false }
  , create
    ( pzem.current()
    , [&env_idx]( float const & c )
      {
        return System::report
        { "overcurrent"
        , env_idx
        , c > current_alert_threshold
          ? System::action::message | System::action::power_off
          : System::action::none 
        };
      }
    , sys
    )
  , create
    ( pzem.power()
    , [&env_idx, &sys]( float const & p )
      {
        bool const fan_psu_no_power
        = env_idx == 0
          && ( isnan( p ) || p == 0 )
          && sys.fan_psu.get_state() != ACVoltageRegulator::state::off;
        
        return System::report
        { "no power"
        , env_idx
        , fan_psu_no_power
          || env_idx == 1
          && ( isnan( p ) || p == 0 )
          && sys.relais_bank.relais_state( RelaisBank::relais_name::main_light )
          ? System::action::message
          : System::action::none
        };
      }
    , sys
    )
  , Value< float >{ pzem.frequency(), false }
  , Value< float >{ pzem.pf(), false }
  };

}
