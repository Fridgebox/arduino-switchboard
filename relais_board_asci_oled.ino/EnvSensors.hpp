# pragma once

# include "common.hpp"

# include <Adafruit_BME280.h>
# include <MQUnifiedsensor.h>
# include <PZEM004Tv30.h>
# include <Wire.h>

struct System;

struct EnvSensors
{
  //- BME280 -
  uint8_t const bme_address;
  Adafruit_BME280 bme;
  constexpr static int8_t humidity_alert_threshold = 85; // [%]
  constexpr static int8_t temp_alert_threshold = 45; // [deg C]

  //- MQ2 -
  constexpr static char const* const mq_board_name = "Arduino NANO";
  constexpr static char const* const mq_sensor_type = "MQ-2";
  constexpr static float const       mq_adc_voltage = 5;
  constexpr static uint8_t const     mq_adc_bit_resolution = 10; // For arduino UNO/MEGA/NANO
  constexpr static float const       mq2_ratio_clean_air = 9.83; //RS / R0 = 9.83 ppm
  MQUnifiedsensor mq2;
  constexpr static auto    gas_lpg_warmup_millis = 3600000;
  constexpr static uint8_t gas_lpg_alert_threshold = 40; // [ppm]

  //- PZEM -
  PZEM004Tv30              pzem;
  constexpr static uint8_t pzem_rx_pin = 10;
  constexpr static uint8_t pzem_tx_pin = 9;
  constexpr static int8_t current_alert_threshold = 4; // [A]

  EnvSensors( uint8_t const bme_address, uint8_t const mq2_analog_pin, uint8_t const pzem_address )
    : bme_address{ bme_address }
    , bme{}
    , mq2
  { mq_board_name
    , mq_adc_voltage
    , mq_adc_bit_resolution
    , mq2_analog_pin
    , mq_sensor_type
  }
  , pzem{ pzem_rx_pin, pzem_tx_pin, pzem_address }
  {
  }

  void setup()
  {

    if ( ! bme.begin( bme_address, &Wire ) )
    {
      error( F("Missing BME280 sensor") );
    }

    /*Set math model to calculate the PPM concentration and the value of constants
      Exponential regression:
      gas_lpg    | a      | b
      H2     | 987.99 | -2.162
      LPG    | 574.25 | -2.222
      CO     | 36974  | -3.109
      Alcohol| 3616.1 | -2.675
      Propane| 658.71 | -2.168
    */
    mq2.setRegressionMethod( 1 ); //_PPM =  a*ratio^b
    mq2.setA( 574.25 );
    mq2.setB( -2.222 ); // Configurate to get LPG concentration

    float calcR0 = 0;

    for ( int i = 1; i <= 10; ++i )
    {
      mq2.update(); // Update data, the arduino will be read the voltage on the analog pin
      calcR0 += mq2.calibrate( mq2_ratio_clean_air );
    }

    mq2.setR0(calcR0 / 10);

    if ( isinf( calcR0 ) )
    {
      error( F("R0 is infite (Open circuit)") );
    }

    if ( calcR0 == 0 )
    {
      error( F("R0 is zero (Analog pin short to ground)" ) );
    }
  }

  template< typename T >
  struct Value
  {
    T const value;
    
    bool has_alert = false;

    Value
    ( T const& v
    , bool const has_alert
    )
    : value{ v }
    , has_alert{ has_alert }
    {}

    operator T const & () const
    {
      return value;
    }
  };

  struct Values
  {
    Value< float > temperature; // [°C]
    Value< float > humidity;// relative [%]
    Value< float > pressure;// [hPa]
    Value< float > gas_lpg;     // [ppm]
    Value< float > ac_voltage;  // [V]
    Value< float > ac_current;  // [A]
    Value< float > ac_power;    // [W]
    Value< float > ac_frequency;// [Hz]
    Value< float > ac_power_factor;// [1]

    static void print_number_or_null( float const & v )
    {
      if ( isnan( v ) )
        Serial.print( F( "null" ) );
      else
        Serial.print( v );
    }

    void print_json( uint8_t const env_idx ) const
    {
      Serial.print( F( "{\"env_index\":") );
      Serial.print( env_idx );
      Serial.print( F( ",\"values\":{\"temperature\":" ) );
      Serial.print( temperature );
      Serial.print( F( ",\"humidity\":" ) );
      Serial.print( humidity );
      Serial.print( F( ",\"pressure\":" ) );
      Serial.print( pressure );
      Serial.print( F( ",\"gas_lpg\":" ) );
      Serial.print( gas_lpg );
      Serial.print( F( ",\"ac_voltage\":" ) );
      print_number_or_null( ac_voltage );
      Serial.print( F( ",\"ac_current\":" ) );
      print_number_or_null( ac_current );
      Serial.print( F( ",\"ac_power\":") );
      print_number_or_null( ac_power );
      Serial.print( F( ",\"ac_frequency\":") );
      print_number_or_null( ac_frequency );
      Serial.print( F(",\"ac_power_factor\":") );
      print_number_or_null( ac_power_factor );
      Serial.println( F("} }") );
    }
  }; // struct Values

  Values
  update( uint8_t const env_idx, System & sys );
}; // struct EnvSensors
