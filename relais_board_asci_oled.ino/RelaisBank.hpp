# pragma once

# include "common.hpp"

struct RelaisBank
{
  static constexpr auto const num_relais = 4;

  static constexpr uint8_t const pins[num_relais] = { 11, 12, 13, 3 };

  static constexpr bool const pin_off_state[num_relais] = { false, false, false, true  };

  enum relais_name
  { main_light = 0
  };

  void setup()
  {
    for ( uint8_t i = 0; i < num_relais; ++i )
    {
      pinMode( pins[ i ], OUTPUT );
      set_relais_state( i, false );
    }
  }

  bool relais_state( uint8_t const idx ) const
  {
    return ( digitalRead( pins[ idx ] ) == HIGH ) ^ pin_off_state[ idx ];
  }

  void set_relais_state( uint8_t const idx, bool const state )
  {
    digitalWrite
    ( pins[ idx ]
    , ( state ^ pin_off_state[ idx ] ) ? HIGH : LOW
    );
  }
};
