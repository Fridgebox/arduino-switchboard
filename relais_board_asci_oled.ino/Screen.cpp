# include "Screen.hpp"
# include "System.hpp"

void Screen::display_status_and_fan_mode( System & sys )
{
  oled.clearField( 0, fan_mode_row, mode_field_width );

  display_field
  ( String( sys.fan_psu.get_state_name() )
  , 1
  , sys.fan_psu.get_state() == ACVoltageRegulator::state::manual
  );

  char const lock_char 
  = sys.emergency_lock_override 
    ? 'O' 
    : sys.emergency_lock
      ? 'L' 
      : ' ';

  display_field
  ( String( lock_char )
  , 1
  , lock_char != ' '
  );

  display_field
  ( String( sys.buzzer.silent_mode ? 's' : ' ' )
  , 1
  , false
  );

  char const alert_char 
  = sys.requested_actions.safe_mode || sys.requested_actions.buzz || sys.requested_actions.power_off
    ? '!' 
    : sys.requested_actions.message 
      ? '?' 
      : ' ';
      
  display_field
  ( String( alert_char )
  , 1
  , alert_char != ' ' && sys.loop_counter % 2
  );

}

void Screen::display_relais_state( RelaisBank const & bank )
{
  char relais_state_str[ RelaisBank::num_relais + 1 ];
  
  relais_state_str[ RelaisBank::num_relais ] = '\0';
  for ( uint8_t relais_idx = 0; relais_idx < RelaisBank::num_relais; ++relais_idx )
  {
    relais_state_str[ relais_idx ] = bank.relais_state( relais_idx ) ? '+' : '-';
  }

  oled.clearField( left_side_column, switch_mode_row, mode_field_width );

  display_field
  ( relais_state_str
  , RelaisBank::num_relais
  , false
  );
}


void Screen::display_status( System const & sys )
{
  oled.setFont( header_font );

  oled.setCursor( status_column, status_row );
  oled.clearToEOL();

  if( System::report current_report = sys.current_report() )
  {
    String msg( current_report.alert );
    msg += " ";
    msg += String( current_report.id );
    display_field
    ( msg 
    , msg.length()
    , true 
    );
    return;
  }

  auto t_up = millis() / 1000;
  char t_unit = 's';
  if( t_up > 3600 )
  {
    t_up /= 3600;
    t_unit = 'h';

    if( t_up > 24 )
    {
      t_up /= 24;
      t_unit = 'd';
    }
  }
  
  //oled.print( F("up") );
  display_field( String( t_up ), 6, t_unit == 's' );
  oled.print( t_unit );
}
