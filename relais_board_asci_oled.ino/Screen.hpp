# pragma once

# include "SSD1306Ascii.h"
# include "SSD1306AsciiWire.h"

# include "RelaisBank.hpp"
# include "ACVoltageRegulator.hpp"
# include "EnvSensors.hpp"

struct Screen
{
private:
  constexpr static int const oled_address = 0x3C;
  
  SSD1306AsciiWire oled;
  
  constexpr static auto const header_font = font8x8;
  constexpr static auto const data_font   = cp437font8x8;
  constexpr static auto const mode_font   = fixed_bold10x15;
  
  constexpr static auto const left_side_column = 0;
  uint8_t power_unit_column;
  uint8_t va_column;
  uint8_t va_unit_column;
  uint8_t status_column;
  
  constexpr static auto const fan_mode_row = 4;
  constexpr static auto const switch_mode_row = 6;
  constexpr static auto const status_row = 3;
  
  constexpr static auto const power_field_width = 3;
  constexpr static auto const mode_field_width = 4;
  constexpr static auto const va_field_width = 3;
  
  constexpr static auto const unit_gap_width = 1;
  
  void display_field
  ( String const & str
  , uint8_t const field_size
  , bool const alert
  )
  { 
    const int8_t num_ws = field_size - str.length();
  
    for ( uint8_t i = 0; i < num_ws; ++i )
      oled.print( ' ' );
  
  
    oled.setInvertMode( alert );
    if( num_ws < 0 )
    {
      oled.print( str.substring( 0, str.length()-3) );

      oled.print( "k" );
    }
    
      oled.print( str );
    oled.setInvertMode( false );
  }
  
  void display_field
  ( float const & val
  , uint8_t const field_size
  , bool const alert = false
  )
  {
    display_field
    ( String( static_cast< int >( val ) )
    , field_size
    , alert
    );
  }
public:  
  void setup()
  {
    Wire.setClock(400000L);

    oled.begin( &Adafruit128x64, oled_address );
  
    oled.displayRemap( analogRead(A7) & 0x01 ? 1 : 0 );

    // Oled static layout
  
    oled.clear();
  
    oled.setFont( mode_font );
  
    power_unit_column = oled.fieldWidth( mode_field_width + power_field_width ) + unit_gap_width;
  
    oled.setFont( data_font );
  
    va_unit_column = power_unit_column + oled.fieldWidth( va_field_width );
  
    oled.setFont( header_font );
  
    va_column = power_unit_column + oled.fieldWidth( 1 ) + unit_gap_width + 2;
  
    va_unit_column += oled.fieldWidth( 1 ) + unit_gap_width * 2 + 2;
  
    oled.setCursor( left_side_column, 0 );
    oled.print( F("  T  H    P   G") );
    
    oled.setCursor( left_side_column, status_row );
    oled.print( F("mode") ); // "mode   pow   V/A"
    status_column = left_side_column + oled.fieldWidth( 5 );
  }
  
  void display_va_units( uint8_t const row )
  {
    uint8_t const base_row = fan_mode_row + row * 2;
  
    oled.setCursor( power_unit_column, base_row + 1  );
    oled.print( F("W") );
  
    oled.setCursor( va_unit_column, base_row );
    oled.print( F("V") );
    oled.setCursor( va_unit_column, base_row + 1 );
    oled.print( F("mA") );
  }
  
  void
  display_env_values
  ( EnvSensors::Values const & v
  , uint8_t const row
  )
  {
    oled.setFont( data_font );
  
    oled.clearField( left_side_column, row + 1, 30 );
  
    display_field( v.temperature.value, 3, v.temperature.has_alert );
  
    display_field( v.humidity, 3, v.pressure.has_alert );
  
    display_field( v.pressure, 5, v.pressure.has_alert );
  
    display_field( v.gas_lpg, 4, v.gas_lpg.has_alert );
  
    auto const va_start_row = fan_mode_row + row * 2;
  
    oled.clearField( va_column, va_start_row, va_field_width );
    display_field
    ( v.ac_voltage
    , va_field_width
    , v.ac_voltage.has_alert
    );
  
    oled.clearField( va_column, va_start_row + 1, va_field_width );
    display_field
    ( v.ac_current * 1000 // in [mA]
    , va_field_width
    , v.ac_current.has_alert
    );
  
    oled.setFont( mode_font );
  
    oled.clearField
    ( oled.fieldWidth( mode_field_width )
    , fan_mode_row + 2 * row
    , power_field_width
    );
  
    display_field
    ( v.ac_power
    , power_field_width
    , v.ac_power.has_alert
    );
  } // display_env_values()
  
  void display_status_and_fan_mode( System & sys );
  
  void display_relais_state( RelaisBank const & bank );
  
  void display_status( System const & report );
  
}; // struct Display  
