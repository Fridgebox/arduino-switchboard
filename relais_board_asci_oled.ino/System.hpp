# pragma once

# include "Screen.hpp"
# include "Buzzer.hpp"
# include "ACVoltageRegulator.hpp"
# include "RelaisBank.hpp"
# include "EnvSensors.hpp"

//# include "common.hpp"

struct System
{
  constexpr static uint8_t const alert_display_duration = 2;
  constexpr static uint8_t const num_env_sensors = 2;
  constexpr static uint8_t const report_queue_size = 4;
  constexpr static unsigned long const heartbeat_interval_ms = 5000;

  using action_mask_t = int8_t;

  enum action : action_mask_t
  { none       = 0
  , message    = 1 << 0
  , safe_mode  = 1 << 1 
  , buzz       = 1 << 2 
  , power_off  = 1 << 3
  };

  struct report
  {    
    char const* alert = nullptr;
    uint8_t id = uint8_t_max;
    action_mask_t action_mask = action::none;

    report
    ( char const* a = nullptr
    , uint8_t i = uint8_t_max
    , action_mask_t m = action::none
    )
    : alert{ a }
    , id{ i }
    , action_mask{ m }
    {}

    void invalidate()
    {
      action_mask = action::none;
    }

    operator bool () const 
    {
      return action_mask;
    }
  };

  struct actions_t
  {
    uint8_t message = 0;
    uint8_t safe_mode = 0;
    uint8_t buzz = 0;
    uint8_t power_off = 0;

    void update( report const & report )
    {
      message   += action::message   & report.action_mask ? 1 : 0;
      safe_mode += action::safe_mode & report.action_mask ? 1 : 0;
      buzz      += action::buzz      & report.action_mask ? 1 : 0;
      power_off += action::power_off & report.action_mask ? 1 : 0;
    }
  };

  Screen screen;
  Buzzer buzzer;

  ACVoltageRegulator fan_psu;
  RelaisBank relais_bank;
  
  EnvSensors env_sensors[ num_env_sensors ] = { { 0x76, A1, 0x07 }, { 0x77, A0, 0x08 } };

  /// Error reports queue
  report reports[ report_queue_size ];
  /// Currently active/displayed error report
  uint8_t current_report_idx  uint8_t_max;

  /// Action requested by reports in the current System::loop() call
  actions_t requested_actions;

  /// Overflowing loop counter
  uint8_t loop_counter = 0;

  /// Last timestamp when heartbeat from controller has been received
  unsigned long last_heartbeat = 0;

  /// Locks command input after severe errors to prevent danger
  bool emergency_lock = false;
  /// Overwrite Emergency Lock
  bool emergency_lock_override = false;

  /// Returns true if emergency lock is in effect
  bool emergency_lock_active() const { return emergency_lock && emergency_lock_override; }

  report const current_report() const 
  {     
    return current_report_idx != uint8_t_max 
           ? reports[ current_report_idx ]
           : report{}; 
  }
  
  void check( report const & report )
  {
    uint8_t found = uint8_t_max;
    uint8_t found_invalid = uint8_t_max;

    for( uint8_t queue_idx = 0; queue_idx < report_queue_size; ++queue_idx )
    {
      if( ! reports[ queue_idx ] )
        found_invalid = queue_idx;

      if( reports[ queue_idx ].alert == report.alert
          && reports[ queue_idx ].id == report.id
        )
        found = queue_idx;
    }
    
    if( found != uint8_t_max && reports[ found ] )
    {
      if( ! report )
      { 
        reports[ found ].invalidate();
       
        if( current_report_idx == found )
          current_report_idx = uint8_t_max;
      }
      return;
    }
    if( ! report )
      return;

    current_report_idx = found_invalid == uint8_t_max ? 0 : found_invalid;

    reports[ current_report_idx ] = report;

    requested_actions.update( report );
  }
  
  uint8_t next_report( uint8_t current_idx )
  {
    for ( uint8_t i = 1; i <= report_queue_size ; ++i )
    {
      uint8_t candidate_idx = ( current_idx + i ) % report_queue_size;
      if( reports[ candidate_idx ] )
        return candidate_idx;
    }
    return uint8_t_max;
  }

  void setup()
  {
    screen.setup();
    
    buzzer.setup();
    
    relais_bank.setup();

    fan_psu.setup();

    // Sensors and VA display preperation
    for ( uint8_t env_idx = 0; env_idx < num_env_sensors; ++env_idx )
    {
      env_sensors[ env_idx ].setup();
      screen.display_va_units( env_idx );
    }

  }

  void print_json_status() const
  {
    Serial.print( F("{\"uptime\":") );
    Serial.print( millis() ); // [ms]
    
    Serial.print( F(",\"emergency_lock\":") );
    print_bool( emergency_lock );
    Serial.print( F(",\"emergency_lock_override\":") );
    print_bool( emergency_lock_override );
    Serial.print( F(",\"silent_mode\":") );
    print_bool( buzzer.silent_mode );
    
    Serial.print( F( ",\"voltage_level\":\"" ) );
    Serial.print( fan_psu.get_state_name() );
    Serial.print( F( "\",\"relais_state\":[" ) );
    for ( uint8_t relais_idx = 0; relais_idx < RelaisBank::num_relais; ++relais_idx )
    {
      print_bool( relais_bank.relais_state( relais_idx ) );
      if( relais_idx + 1 < RelaisBank::num_relais )
        print_comma();
    }
    Serial.print( F("],\"reports\":[") );

    bool first_report = true;
    for( uint8_t report_idx = 0; report_idx < report_queue_size; ++report_idx )
    {
      report const& rep = reports[ report_idx ];

      if( ! rep ) continue;
      
      if( ! first_report )
        print_comma();
      
      Serial.print( F("{\"alert\":\"") );
      Serial.print( rep.alert );
      Serial.print( F( "\", \"id\":" ) );
      Serial.print( rep.id );
      Serial.print( F( "}" ) );

      first_report= false;
    }
    Serial.print( F("],\"actions\":{\"message\":") );
    Serial.print( requested_actions.message );
    Serial.print( F(",\"safe_mode\":") );
    Serial.print( requested_actions.safe_mode );
    Serial.print( F(",\"buzz\":") );
    Serial.print( requested_actions.buzz );
    Serial.print( F(",\"power_off\":") );
    Serial.print( requested_actions.power_off );
    Serial.println( F("}}") );
    
  }

  void handle_input()
  {
    //constexpr unsigned short const fan_switch_delay = 1900;
    //constexpr unsigned short const bank_switch_delay = 500;
    constexpr uint8_t  const input_buffer_max_size = 4;
    
    char input_buffer[ input_buffer_max_size ];
  
    uint8_t const input_buffer_size = Serial.readBytesUntil( '\n', input_buffer, input_buffer_max_size );
  
    if( input_buffer_size == 0 )
      return;
  
    uint8_t idx = 0;
    
    // Override from emergency lock for current command
    if( input_buffer[0] == '!' )
      ++idx; 

    if( input_buffer[0] == '~' )
    {
      last_heartbeat = millis();
      return;
    }
  
    char const cmd = input_buffer[ idx++ ];
  
    if( input_buffer_size < idx )
    { error( F("missing input" ) );
      return;
    }
  
    if( cmd == 'S' )
    {
      switch( input_buffer[ idx ] )
      {
        case 'L': emergency_lock = true; break;
        case 'l': emergency_lock = false; break;
        case 'O': emergency_lock_override = true; break;
        case 'o': emergency_lock_override = false; break;
        case 'S': buzzer.silent_mode = true; break;
        case 's': buzzer.silent_mode = false; break;
      }
      return;
    }
    
    //  idx > 1 -> Local Emergency Override active
    if( idx > 1 && emergency_lock_active() )
    {
      error( F("emergency: ignoring input" ) );
      return;
    }
  
    // Voltage level
    if( cmd == 'V' )
    {
      //bool prev_off_state = fan_psu.get_state() == ACVoltageRegulator::state::off;
      
      fan_psu.set( input_buffer[idx] );
      
      //delay( prev_off_state * fan_switch_delay );
      return;
    }
    // Relais bank  
    if( cmd == 'R' )
    {
      if( input_buffer_size < idx + 2  )
      {
        error( F("input missing" ) );
        return;
      }
  
      uint8_t const nr = input_buffer[idx++] - '0';
      bool const requested_state = input_buffer[idx] - '0';
  
      relais_bank.set_relais_state( nr, requested_state );
      
      // Allow power consumption increase to avoid no consumer error
      //delay( requested_state * bank_switch_delay );
      return;
    }
  
    error( F("unknown command") );
  }

  void handle_reports()
  {
    if
    ( ++ loop_counter % alert_display_duration == 0
    ||  current_report_idx == uint8_t_max 
    ) current_report_idx = next_report( current_report_idx );


    if( requested_actions.safe_mode  )
    {
      fan_psu.set( ACVoltageRegulator::state::manual );
      // TODO: enable secondary lights
      //relais_bank.setup(); // toggle_off
    }

    if( requested_actions.power_off > 1 )
    {
      emergency_lock = true;
      fan_psu.set( ACVoltageRegulator::state::off );
      relais_bank.setup(); // toggle_off
    }

    if( requested_actions.buzz > 1 )
      buzzer.activate();
    else
      buzzer.deactivate();

  
  }

  void loop()
  {
    requested_actions = {};

    handle_input();

    check
    ( report
      { "heartbeat"
      , 0
      , millis() - last_heartbeat > heartbeat_interval_ms
        ? action::message | action::safe_mode
        : action::none
      }
    );

    for( uint8_t env_idx = 0; env_idx < num_env_sensors; ++env_idx )
    {
      auto const values
      = env_sensors[env_idx].update
        ( env_idx
        , *this
        );
  
      values.print_json( env_idx );
  
      screen.display_env_values( values, env_idx );
    }
  
    handle_reports();

    print_json_status();

    screen.display_status( *this );

    screen.display_status_and_fan_mode( *this );

    screen.display_relais_state( relais_bank );

    buzzer.loop();
  }
};
