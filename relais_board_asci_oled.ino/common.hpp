# pragma once

# include <Arduino.h>

constexpr uint8_t const uint8_t_max = 255;
constexpr uint16_t const uint16_t_max = 65535;
constexpr unsigned long const unsigned_long_max = 4294967295;

inline void error( __FlashStringHelper const *  msg )
{
  Serial.print( F( "E:" ) );
  Serial.println( msg );
}

inline void print_comma()
{
  Serial.print( F(",") );
}

inline void print_bool( bool const b )
{
  Serial.print( b ? F("true") : F("false") );
}
