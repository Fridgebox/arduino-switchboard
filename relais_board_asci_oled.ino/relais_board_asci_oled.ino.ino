# include "System.hpp"

System sys;

void setup()
{
  Serial.begin( 115200 );
  Wire.begin();
  Wire.setClock(400000L);

  sys.setup();
}

void loop()
{
  constexpr unsigned short int const step_delay = 100;

  //delay( step_delay);

  sys.loop();
}
